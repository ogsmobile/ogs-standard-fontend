<p style='text-align: right;'> อัพเดทวันที่ 21 กุมภาพันธุ์ 2565 </p>

---

# One Geo Survey
##### Standard Frontend

---
### JavaScript Styles Guild

[Airbnb JavaScript Styles Guild](https://github.com/airbnb/javascript)
```
ใช้หลักการเขียน Javascript, React, CSS & Sass
```

### Naming Conventions Override

#### 1. Directory and File

* #### Function filename ( camelCase )

```
รูปแบบการตั้งชื่อไฟล์ในกรณีที่ไฟล์นั้นเป็นฟังชั่นให้ตั้งชื่อในรูปแบบของ camelCase

ตัวอย่าง
- getSomething.js
- sumCalendar.tsx
```

* #### Component filename ( PascalCase )
```
รูปแบบการตั้งชื่อไฟล์ในกรณีที่ไฟล์นั้นเป็นฟังชั่นให้ตั้งชื่อในรูปแบบของ PascalCase

ตัวอย่าง
- InputFile.js
- DatePickerModal.jsx
```
* #### Constants filename ( PascalCase )
```
รูปแบบการตั้งชื่อไฟล์ในกรณีที่ไฟล์นั้นเก็บตัวแปรในรูปแบบของค่าคงที่ให้ตั้งชื่อในรูปแบบ PascalCase

ตัวอย่าง
- Config.js
- PathUrl.jsx
```
* #### CSS filename ( PascalCase )
```
รูปแบบการตั้งชื่อไฟล์ในกรณีที่เก็บ css ให้ตั้งชื่อในรูปแบบของ PascalCase

ตัวอย่าง
- Main.css
- Dashboard.scss
```
* #### Component Directory ( PascalCase )
```
โฟร์เดอร์ที่เก็บ Component ให้ตั้งชื่อในรูปแบบของ PascalCase

ตัวอย่าง
- MainScreen
- DashboardStorePage
```
* #### Common Directory ( camelCase )
```
โฟร์เดอร์ที่เก็บข้อมูลทั่วไป ให้ตั้งชื่อในรูปแบบของ camelCase

ตัวอย่าง
- store
- utils
- pageScreen
```

#### 2. Git Naming

* #### Name ( kebab-case )
```
การตั้งชื่อ branch ให้ตั้งรูปแบบของ Kabab-case

ตัวอย่าง
- datepicker-component
```

* #### Groups name 

```
การแบ่ง Group ให้แบ่งโดยใช้เครื่องหมาย /

ตัวอย่าง
- home-page/future/datepicker-component
```

#### 3. Variable Naming

* #### Variable ( snake_case )

```
การตั้งชื่อตัวแปรให้ตั้งอยู่ในรูปแบบของ snake_case

ตัวอย่าง
let status_list
const form_state
```

* #### Function Variable ( camelCase )

```
การตั้งชื่อฟังก์ชันให้ตั้งอยู่ในรูปแบบของ camelCase
```
```
การตั้งชื่อเติม fn เข้าไปด้านหน้าเพื่อเป็นการระบุว่าเป็นฟังก์ชันของเราเอง

ตัวอย่าง
function fnSetStatusList()
```

* #### React Hook State
```
- ส่วนของ state ให้ตั้งอยู่ใน snake_case 
- ส่วนของ setState ให้ตั้งในรูปแบบของ camelCase และมีเติม set ด้านหน้า 

ตัวอย่าง
const [ state , setState ] = useState()
```

* #### Constants 

```
ค่าคงที่ให้ต้ั้งชื่อตัวแปรในรูปแบบของ SNAKE_UPPERCASE

ตัวอย่าง
const PATH_URL = "https://api.com/"
```
### Code Formatter

ใช้เครื่องมือ Prettier ในการจัดโค้ด

### Code Validation

ใช้เครื่องมือ ESlint ในการจัดโค้ด
